package com.bstek.datasource.bean;

public class DataSourceVO {

	/**
	 * 数据源唯一标识
	 */
	private String code;

	/**
	 * 数据源显示名称
	 */
	private String name;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
