/*******************************************************************************
 * Copyright 2017 Bstek
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.bstek.designer.action;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bstek.common.exception.ReportDesignException;
import com.bstek.common.utils.StringUtils;
import com.bstek.designer.bean.PreviewPageParameters;
import com.bstek.designer.bean.PreviewParameters;
import com.bstek.designer.bean.PreviewSearchForm;
import com.bstek.shiro.JWTUtil;
import com.bstek.shiro.TokenFilter;
import com.bstek.ureport.build.ReportBuilder;
import com.bstek.ureport.build.paging.Page;
import com.bstek.ureport.definition.Paper;
import com.bstek.ureport.definition.ReportDefinition;
import com.bstek.ureport.exception.ReportComputeException;
import com.bstek.ureport.exception.ReportException;
import com.bstek.ureport.export.ExportUtils;
import com.bstek.ureport.export.PageBuilder;
import com.bstek.ureport.export.ProducerEnum;
import com.bstek.ureport.export.ReportRender;
import com.bstek.ureport.export.SinglePageData;
import com.bstek.ureport.export.html.HtmlProducer;
import com.bstek.ureport.export.html.HtmlReport;
import com.bstek.ureport.export.html.SearchFormData;
import com.bstek.ureport.model.Report;
import com.bstek.ureport.provider.ProviderFactory;
import com.bstek.ureport.provider.report.ReportProvider;
import com.github.benmanes.caffeine.cache.Cache;

/**
 * @author Jacky.gao
 * @since 2017年2月15日
 */
@RestController
@RequestMapping("/report/preview")
public class ReportPreviewAction {

	@Resource
	Cache<String, Report> caffeineCache;

	private ReportRender reportRender = new ReportRender();

	private HtmlProducer htmlProducer = new HtmlProducer();

	@RequestMapping("")
	@ResponseBody
	public HtmlReport loadData(HttpServletRequest request, @RequestBody PreviewPageParameters pageParameters) {
		PreviewParameters params = pageParameters.getParams();
		Map<String, Object> parameters = params.getQuery();
		if(parameters == null) {
			parameters = new HashMap<String, Object>();
		}
		// 相同的低优先级的参数会被高优先级参数覆盖
		// 参数优先级， token参数 > 页面表单参数 > url链接参数 > 数据集默认参数
		String token = request.getHeader(TokenFilter.X_TOKEN);
		parameters.putAll(JWTUtil.getParams(token));
		HtmlReport htmlReport = new HtmlReport();
		ReportDefinition reportDefinition = null;
		String reportName = params.getReportName();
		String content = params.getContent();
		if (StringUtils.isNotBlank(reportName)) {
			reportDefinition = reportRender.getReportDefinition("file:" + reportName);
		} else if (StringUtils.isNotBlank(content)) {
			content = StringUtils.decode(content);
			System.out.println(content);
			reportDefinition = reportRender.getReportDefinition(content, "utf-8");
		} else {
			throw new ReportComputeException("Report file can not be null.");
		}
		if (reportDefinition == null) {
			throw new ReportDesignException("Report data has expired,can not do preview.");
		}
		Paper paper = reportDefinition.getPaper();
		Report report = null;
		String html = "";
		if (paper.isPageEnabled()) { // 分页
			// 通过读取文件分页，需要判断文件是否被修改，防止加载修改前的缓存数据
			if (StringUtils.isNotBlank(reportName)) {
				ReportProvider reportProvider = ProviderFactory.getFileReportProvider();
				try (InputStream in = reportProvider.loadReport("file:" + reportName)){
					String uuid = DigestUtils.md5Hex(in);
					params.setSign(uuid);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			String key = DigestUtils.md5Hex(params.toString());
			report = caffeineCache.getIfPresent(key);
			if (report == null) {
				report = reportRender.render(reportDefinition, parameters);
				caffeineCache.put(key, report);
			}
			int pageIndex = pageParameters.getPageIndex();
			SinglePageData pageData = PageBuilder.buildSinglePageData(pageIndex, report);
			List<Page> pages = pageData.getPages();
			int totalPage = pageData.getTotalPages();
			boolean isLastPage = false;
			if(pageIndex == totalPage) {
				isLastPage = true;
			}
			html = htmlProducer.produce(report.getContext(), pages, pageData.getColumnMargin(), false, isLastPage);
			htmlReport.setTotalPage(pageData.getTotalPages());
			htmlReport.setPageIndex(pageIndex);
			if (reportDefinition.getPaper().isColumnEnabled()) {
				htmlReport.setColumn(reportDefinition.getPaper().getColumnCount());
			}
		} else {
			report = reportRender.render(reportDefinition, parameters);
			html = htmlProducer.produce(report);
		}
		htmlReport.setChartDatas(report.getContext().getChartDataMap().values());
		htmlReport.setContent(html);
		htmlReport.setStyle(reportDefinition.getStyle());
		SearchFormData searchFormData = reportDefinition.buildSearchFormData(report.getContext().getDatasetMap(),
				parameters);
		htmlReport.setSearchFormData(searchFormData);
		htmlReport.setReportAlign(report.getPaper().getHtmlReportAlign().name());
		return htmlReport;
	}

	@RequestMapping("/refreshForm")
	public Map<String, Object> previewRefreshForm(@RequestBody PreviewSearchForm form) {
		String content = form.getContent();
		String reportName = form.getReportName();
		ReportDefinition reportDefinition = null;
		if (StringUtils.isNotBlank(reportName)) {
			reportDefinition = reportRender.getReportDefinition("file:" + reportName);
		} else if (StringUtils.isNotBlank(content)) {
			content = StringUtils.decode(content);
			System.out.println(content);
			reportDefinition = reportRender.getReportDefinition(content, "utf-8");
		} else {
			throw new ReportComputeException("Report file can not be null.");
		}
		if (reportDefinition == null) {
			throw new ReportDesignException("Report data has expired,can not do preview.");
		}
		return reportDefinition.refreshSearchForm(form.getItem(), form.getQuery());
	}

	@RequestMapping("/download/{type}")
	public void buildExcel(HttpServletRequest request, @PathVariable String type, @RequestBody PreviewParameters reportParameters,HttpServletResponse response) {
		long start = System.currentTimeMillis();
		String content = reportParameters.getContent();
		String reportName = reportParameters.getReportName();
		ReportDefinition reportDefinition = null;
		if (StringUtils.isNotBlank(reportName)) {
			reportDefinition = reportRender.getReportDefinition("file:" + reportName);
		} else if (StringUtils.isNotBlank(content)) {
			content = StringUtils.decode(content);
			System.out.println(content);
			reportDefinition = reportRender.getReportDefinition(content, "utf-8");
		} else {
			throw new ReportComputeException("Report file can not be null.");
		}
		if (reportDefinition == null) {
			throw new ReportDesignException("Report data has expired,can not do preview.");
		}
		Map<String, Object> parameters = reportParameters.getQuery();
		if(parameters == null) {
			parameters = new HashMap<String, Object>();
		}
		// 相同的低优先级的参数会被高优先级参数覆盖
		// 参数优先级， token参数 > 页面表单参数 > url链接参数 > 数据集默认参数
		String token = request.getHeader(TokenFilter.X_TOKEN);
		parameters.putAll(JWTUtil.getParams(token));
		ReportBuilder reportBuilder = reportRender.getReportBuilder();
		Report report = reportBuilder.buildReport(reportDefinition, parameters);
		Map<String, String> chartImages = reportParameters.getChartImages();
		report.setChartImages(chartImages);
		try {
			OutputStream out = response.getOutputStream();
			response.setCharacterEncoding("UTF-8");
			response.setContentType("application/octet-stream; charset=UTF-8");
			// 创建下载文件
			ProducerEnum p = ProducerEnum.valueOf(type.toUpperCase());
			String downFileName = StringUtils.randomFileName() + StringUtils.getFileSuffix(p);
			response.setHeader("Content-Disposition",
					"attachment; filename=" + URLEncoder.encode(downFileName, "UTF-8"));
			response.setHeader("code", "20000");
			ExportUtils.export(out, report, p);
			long end = System.currentTimeMillis();
			System.out.println(end - start);
		} catch (Exception ex) {
			throw new ReportException(ex);
		}
	}
}
