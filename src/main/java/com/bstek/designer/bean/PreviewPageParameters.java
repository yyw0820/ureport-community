package com.bstek.designer.bean;

public class PreviewPageParameters {

	private int pageIndex;
	
	private PreviewParameters params;

	public int getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(int pageIndex) {
		this.pageIndex = pageIndex;
	}

	public PreviewParameters getParams() {
		return params;
	}

	public void setParams(PreviewParameters params) {
		this.params = params;
	}
}
