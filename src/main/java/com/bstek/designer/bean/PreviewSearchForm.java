package com.bstek.designer.bean;

import java.util.Map;

/**
 * 刷新表单数据
 * 主要用于出现表单级联选择问题
 * @author Administrator
 *
 */
public class PreviewSearchForm {

	/**
	 * 报表内容
	 */
	private String content;
	
	/**
	 * 报表名称
	 */
	private String reportName;
	
	/**
	 * 当前变更条件
	 */
	private Map<String, Object> item;
	
	/**
	 * 表单参数
	 */
	private Map<String, Object> query;

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Map<String, Object> getItem() {
		return item;
	}

	public void setItem(Map<String, Object> item) {
		this.item = item;
	}

	public Map<String, Object> getQuery() {
		return query;
	}

	public void setQuery(Map<String, Object> query) {
		this.query = query;
	}

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}
}
