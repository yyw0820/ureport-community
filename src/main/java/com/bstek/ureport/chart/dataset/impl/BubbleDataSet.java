package com.bstek.ureport.chart.dataset.impl;

/**
 * 气泡图
 */
public class BubbleDataSet extends ScatterDataSet{

	@Override
	public String getType() {
		return "bubble";
	}
}
