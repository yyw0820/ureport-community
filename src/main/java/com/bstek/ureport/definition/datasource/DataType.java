/*******************************************************************************
 * Copyright 2017 Bstek
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.bstek.ureport.definition.datasource;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.bstek.common.utils.StringUtils;
import com.bstek.ureport.Utils;
import com.bstek.ureport.exception.ReportComputeException;

/**
 * @author Jacky.gao
 * @since 2016年12月27日
 */
public enum DataType {

	Integer, Float, Boolean, String, Date, List;

	public Object parse(Object obj) {
		String str = StringUtils.toString(obj);
		switch (this) {
		case Boolean:
			if (StringUtils.isBlank(str)) {
				return null;
			}
			if (obj instanceof Boolean) {
				return (Boolean) obj;
			} 
			return java.lang.Boolean.valueOf(str);
			
		case Float:
			if (StringUtils.isBlank(str)) {
				return null;
			}
			if (obj instanceof Float) {
				return (Float) obj;
			}
			return Utils.toBigDecimal(obj).doubleValue();
		case Integer:
			if (StringUtils.isBlank(str)) {
				return null;
			}
			if (obj instanceof Integer) {
				return (Integer) obj;
			}
			return Utils.toBigDecimal(obj).intValue();
		case String:
			return str;
		case List:
			if (StringUtils.isBlank(str)) {
				return null;
			}
			if (obj instanceof List) {
				return (List<?>) obj;
			}
			String[] arrs = obj.toString().split(",");
			List<String> list = new ArrayList<String>();
			for (int i = 0; i < arrs.length; i++) {
				list.add(arrs[i]);
			}
			return list;
		case Date:
			if (StringUtils.isBlank(str)) {
				return new Date();
			}
			if (obj instanceof Date) {
				return (Date) obj;
			}
			try {
				return StringUtils.parseDate(str);
			} catch (ParseException e) {
				throw new ReportComputeException("Date parameter value pattern must be \"yyyy\" or \"yyyy-MM\" or \"yyyy-MM-dd\" or \"yyyy-MM-dd HH:mm:ss\".");
			}
		}
		throw new ReportComputeException("Unknow parameter type : " + this);
	}
}
