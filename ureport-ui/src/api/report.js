import request from '@/utils/request'

export function loadData(params,token) {
  return request({
    url: '/report/preview',
    method: 'post',
    headers: {'X-Token': token},
    data:params
  })
}

export function downloadWord(params,token) {
  return request({
    url: '/report/preview/download/word',
    method: 'post',
    headers: {'X-Token': token},
    responseType: 'arraybuffer',
    data:params
  })
}

export function downloadExcel(params,token) {
  return request({
    url: '/report/preview/download/excel',
    method: 'post',
    headers: {'X-Token': token},
    responseType: 'arraybuffer',
    data:params
  })
}

export function downloadPDF(params,token) {
  return request({
    url: '/report/preview/download/pdf',
    method: 'post',
    headers: {'X-Token': token},
    responseType: 'arraybuffer',
    data:params
  })
}

export function refreshSearchForm(params,token) {
  return request({
    url: '/report/preview/refreshForm',
    method: 'post',
    headers: {'X-Token': token},
    data:params
  })
}
