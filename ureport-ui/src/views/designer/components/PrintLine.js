/**
 * Created by Jacky.Gao on 2017-02-18.
 */
import {mmToPoint} from './Utils.js'

export default class PrintLine {
    constructor(context) {
      context.printLine = this
      this.context = context
      this.refresh()
    }
    refresh(){
      const paper = this.context.reportDef.paper
      const orientation=paper.orientation
      let width = paper.width
      if(orientation ==='landscape'){
          width = paper.height
      }
      width = width - paper.leftMargin - paper.rightMargin + 38
      const elementId = this.context.elementId
      const ele = document.getElementById(elementId)
      const height = ele.offsetHeight
      const print = ele.nextElementSibling
      debugger
      if(width > ele.clientWidth) {
        print.style.display = 'none'
      } else {
        print.style.display = ''
      }
      print.style.left = width + 'pt'
      print.style.height = height - 46 + 'px'
    }
};
