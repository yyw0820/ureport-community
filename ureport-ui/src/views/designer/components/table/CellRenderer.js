/**
 * Created by Jacky.Gao on 2017-01-31.
 */
import ChartWidget from '../widget/ChartWidget.js'

import imagePath from '@/assets/icons/image.svg'
import qrcodePath from '@/assets/icons/qrcode.svg'
import barcodePath from '@/assets/icons/barcode.svg'

import exprExpandDown from '@/assets/icons/expr-expand-down.svg';
import expandDown from '@/assets/icons/expand-down.svg';

import exprExpandRight from '@/assets/icons/expr-expand-right.svg';
import expandRight from '@/assets/icons/expand-right.svg';

import property from '@/assets/icons/property.svg';
import expression from '@/assets/icons/expression.svg';

export function afterRenderer(td,row,col,prop,value,cellProperties){
    if(!this.context){
      return;
    }
    let cellDef = this.context.getCell(row,col)
    if(!cellDef) {
        return;
    }
    let isDefault = true
    const cellStyle = cellDef.cellStyle
    isDefault = isDefault && (Object.keys(cellStyle).length === 0 || (Object.keys(cellStyle).length === 4 && cellStyle.align === 'left' && cellStyle.fontSize == '10' && cellStyle.lineHeight == 0 && cellStyle.valign == 'middle'))
    const cellValue = cellDef.value
    isDefault = isDefault &&   Object.keys(cellValue).length === 2 && cellValue.value === '' && cellValue.type == 'simple'
    if(isDefault) {
        return;
    }
    const valueType = cellValue.type
    td.innerHTML = ''
    if(valueType==='dataset') {
        const image = document.createElement('img')
        if(cellDef.expand === 'Down') {
          image.src = expandDown
        } else if(cellDef.expand === 'Right') {
          image.src = expandRight
          image.style.position = 'absolute'
        } else {
          image.src = property
        }
        td.appendChild(image)
        const tip = cellValue.datasetName + "." + cellValue.aggregate+ '(' + cellValue.property + ')'
        const text = document.createTextNode(tip)
        td.appendChild(text)
    } else if(valueType==='expression'){
        const image = document.createElement('img')
        if(cellDef.expand === 'Down') {
          image.src = exprExpandDown
        } else if(cellDef.expand === 'Right') {
          image.src = exprExpandRight
          image.style.position = 'absolute'
        } else {
          image.src = expression
        }
        td.appendChild(image)
        const tip= cellValue.value || ''
        const text = document.createTextNode(tip)
        td.appendChild(text)
    } else if(valueType==='image') {
        const image = document.createElement('img')
        image.src = imagePath
        image.width = 20
        td.appendChild(image)
    } else if(valueType==='slash'){
        const container = document.createElement('div')
        container.style.width = td.clientWidth + 'px'
        container.style.height =  td.clientHeight + 'px'
        let svgXml = cellDef.value.svgXml
        if(svgXml){
          container.innerHTML = svgXml
        }
        td.appendChild(container)
    } else if(valueType==='zxing'){
        let imagePath = qrcodePath
        if(cellValue.category==='barcode') {
          imagePath = barcodePath
        }
        const image = document.createElement('img')
        image.src = imagePath
        image.width =  cellValue.width
        image.height = cellValue.height
        td.appendChild(image)
    } else if(valueType==='chart') {
        if(!cellDef.chartWidget) {
          cellDef.chartWidget = new ChartWidget(td,cellDef)
        }
        cellDef.chartWidget.renderChart(td,this.context,row,col)
    } else if(valueType==="simple"){
        let tip = cellValue.value || ""
        if(tip && tip!==""){
            tip = tip.replace(new RegExp('<','gm'),'&lt;')
            tip = tip.replace(new RegExp('>','gm'),'&gt;')
            tip = tip.replace(new RegExp('\r\n','gm'),'<br>')
            tip = tip.replace(new RegExp('\n','gm'),'<br>')
            tip = tip.replace(new RegExp(' ','gm'),'&nbsp;')
            const text = document.createTextNode(tip)
            td.appendChild(text)
        }
    }
    td.style.wordBreak = 'break-all'
    td.style.lineHeight = 'normal'
    td.style.whiteSpace = 'nowrap'
    td.style.padding = '0 1px'
    if(cellStyle.align) {
      td.style.textAlign = cellStyle.align
    }
    if(cellStyle.valign) {
      td.style.verticalAlign = cellStyle.valign
    }
    if(cellStyle.bold) {
      td.style.fontWeight = 'bold'
    }
    if(cellStyle.italic) {
      td.style.fontStyle = 'italic'
    }
    if(cellStyle.underline) {
      td.style.textDecoration = 'underline'
    }
    if(cellStyle.forecolor){
      td.style.color = "rgb("+cellStyle.forecolor+")"
    }
    if(cellStyle.bgcolor){
      td.style.backgroundColor = "rgb("+cellStyle.bgcolor+")"
    }
    if(cellStyle.fontSize) {
      td.style.fontSize = cellStyle.fontSize+"pt"
    }
    if(cellStyle.fontFamily){
      td.style.fontFamily = cellStyle.fontFamily
    }
    if(cellStyle.lineHeight){
      td.style.lineHeight = cellStyle.lineHeight
    }else{
      td.style.lineHeight = ''
    }
    const leftBorder = cellStyle.leftBorder
    if(leftBorder) {
        if(leftBorder === '' || leftBorder.style === "none") {
          td.style.borderLeft = ''
        } else {
          let borderStyle = leftBorder.style ? leftBorder.style : 'solid'
          let borderWidth = leftBorder.width ? parseInt(leftBorder.width) : 0
          if (borderStyle === 'solid') {
            borderStyle = 'double'
          } else {
            borderWidth += 0.5
          }
          const style = borderWidth + "px " + borderStyle  + " rgb(" + leftBorder.color + ")"
          td.style.borderLeft = style
        }
    }
    const rightBorder = cellStyle.rightBorder
    if(rightBorder) {
        if(rightBorder==='' || rightBorder.style==="none") {
          td.style.borderRight = ''
        }else{
          let borderStyle = rightBorder.style ? rightBorder.style : 'solid'
          let borderWidth = rightBorder.width ? parseInt(rightBorder.width) : 0
          if (borderStyle === 'solid') {
            borderStyle = 'double'
          } else {
            borderWidth += 0.5
          }
          let style = borderWidth + "px " + borderStyle + " rgb("+ rightBorder.color+")"
          td.style.borderRight = style
        }
    }
    const topBorder = cellStyle.topBorder;
    if(topBorder){
      if(topBorder ==='' || topBorder.style==="none"){
        td.style.borderTop = ''
      } else {
        let borderStyle = topBorder.style ? topBorder.style : 'solid'
        let borderWidth = topBorder.width ? parseInt(topBorder.width) : 0
        if (borderStyle === 'solid') {
          borderStyle = 'double'
        } else {
          borderWidth += 0.5
        }
        let style = borderWidth + "px " + borderStyle + " rgb("+ topBorder.color+")"
        td.style.borderTop = style
      }
    }
    const bottomBorder = cellStyle.bottomBorder
    if(bottomBorder) {
      if(bottomBorder === '' || bottomBorder.style === "none") {
        td.style.borderBottom = ''
      }else{
        let borderStyle = bottomBorder.style ? bottomBorder.style : 'solid'
        let borderWidth = bottomBorder.width ? parseInt(bottomBorder.width) : 0
        if (borderStyle === 'solid') {
          borderStyle = 'double'
        } else {
          borderWidth += 0.5
        }
        let style = borderWidth + "px " + borderStyle + " rgb("+ bottomBorder.color+")"
        td.style.borderBottom = style
      }
    }
}
